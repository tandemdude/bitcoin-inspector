import typing

import bitcoin_inspector.http as http


def convert_satoshi_to_bitcoin(amount: int) -> float:
    """
    Convert a satoshi amount to its bitcoin equivalent.

    Args:
        amount (:obj:`int`): Satoshi amount to convert.

    Returns:
        :obj:`float` bitcoin equivalent.
    """
    return amount / 100000000


def get_bitcoin_exchange_rate(currency: str) -> float:
    """
    Get the current exchange rate from bitcoin to a fiat currency.
    Available currencies can be found `here <https://www.blockchain.com/api/exchange_rates_api>`_.

    Args:
        currency (:obj:`str`): The currency to get the exchange rate for.

    Returns:
        :obj:`float` value of one bitcoin in the specified currency.

    Example:

        .. code-block:: python

            >>> get_bitcoin_exchange_rate("GBP")
            8682.53
    """
    data = http.get_exchange_rates()
    return data[currency.upper()]["last"]
