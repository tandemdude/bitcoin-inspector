from __future__ import annotations
import typing

import bitcoin_inspector.http as http


def get_transaction(transaction_hash: str) -> Transaction:
    """
    Get a :obj:`.Transaction` object for a given bitcoin transaction hash.
    The returned object will contain lists of the inputs and outputs for that transaction.

    Args:
        transaction_hash (:obj:`str`): The transaction hash to fetch the object for.

    Returns:
        :obj:`.Transaction` object representing the bitcoin transaction.
    """
    return Transaction.from_dict(http.get_raw_transaction(transaction_hash))


class TransactionInput:
    """
    Represents an input to a bitcoin transaction.

    Args:
        payload (:obj:`dict`): Json payload from which the object was built.
        address (:obj:`str`): Hex bitcoin address the input came from.
        value (:obj:`int`): Input amount in Satoshi.
    """

    def __init__(self, payload: dict, address: str, value: int):
        self.payload = payload
        self.address = address
        self.value = value

    @classmethod
    def from_dict(cls, payload: dict) -> TransactionInput:
        """
        Instantiate a :obj:`.TransactionInput` object from the json payload supplied by the blockchain API.

        Args:
            payload (:obj:`dict`): JSON data received from the blockchain API.

        Returns:
            :obj:`.TransactionInput` object representing the transaction input.
        """
        return cls(payload, payload["prev_out"]["addr"], payload["prev_out"]["value"],)


class TransactionOutput:
    """
    Represents an output from a bitcoin transaction.

    Args:
        payload (:obj:`dict`): Json payload from which the object was built.
        address (:obj:`str`): Hex bitcoin address the output was sent to.
        value (:obj:`int`): Output amount in Satoshi.
    """

    def __init__(self, payload: dict, address: str, value: int):
        self.payload = payload
        self.address = address
        self.value = value

    @classmethod
    def from_dict(cls, payload: dict) -> TransactionOutput:
        """
        Instantiate a :obj:`.TransactionOutput` object from the json payload supplied by the blockchain API.

        Args:
            payload (:obj:`dict`): JSON data received from the blockchain API.

        Returns:
            :obj:`.TransactionOutput` object representing the transaction output.
        """
        return cls(payload, payload["addr"], payload["value"])


class Transaction:
    """
    Represents a bitcoin transaction.

    Args:
        payload (:obj:`dict`): Json payload from which the object was built.
        transaction_hash (:obj:`str`): The hex hash of the bitcoin transaction.
        size (:obj:`int`): The size of the transaction in bytes.
        block_height (:obj:`int`): The height of the block the transaction is in.
        inputs (:obj:`typing.List` [ :obj:`.TransactionInput` ]): The inputs to the transaction.
        outputs (:obj:`typing.List` [ :obj:`.TransactionOutput` ]): The outputs from the transaction.
    """

    def __init__(
        self,
        payload: dict,
        transaction_hash: str,
        size: int,
        block_height: int,
        inputs: typing.List[TransactionInput],
        outputs: typing.List[TransactionOutput],
    ):
        self.payload = payload
        self.hash = transaction_hash
        self.hyperlink = f"https://www.blockchain.com/btc/tx/{self.hash}"
        self.size = size
        self.block_height = block_height
        self.inputs = inputs
        self.outputs = outputs
        self.fee = self.calculate_transaction_fee()

    @classmethod
    def from_dict(cls, payload: dict) -> Transaction:
        """
        Instantiate a :obj:`.Transaction` object from the json payload supplied by the blockchain API.

        Args:
            payload (:obj:`dict`): JSON data received from the blockchain API.

        Returns:
            :obj:`.Transaction` object representing the bitcoin transaction.
        """
        return cls(
            payload,
            payload["hash"],
            payload["size"],
            payload["block_height"],
            [TransactionInput.from_dict(tx_input) for tx_input in payload["inputs"]],
            [TransactionOutput.from_dict(tx_output) for tx_output in payload["out"]],
        )

    def calculate_transaction_fee(self) -> int:
        """
        Calculates the fee for the transaction from the sum of output amounts subtracted
        from the sum of the input amounts.

        Returns:
            :obj:`int` Satoshi fee on the transaction
        """
        input_sum = sum(tx_input.value for tx_input in self.inputs)
        output_sum = sum(tx_output.value for tx_output in self.outputs)
        return input_sum - output_sum

    @property
    def confirmations(self) -> int:
        """
        Calculates the number of confirmations on the transaction.
        Calculated by subtracting the transaction's block height from
        the height of the current block.

        Returns:
            :obj:`int` number of confirmations on the transaction.
        """
        return http.get_current_block_height() - self.block_height
