from __future__ import annotations

import typing

import bitcoin_inspector.http as http
from .transaction import Transaction


def get_address(address_hash: str) -> Address:
    """
    Get an :obj:`.Address` object for a given bitcoin address hash.
    The returned object will contain a list of up to 50 transactions for that address at the time. If you wish to check
    for new transactions you can call :meth:`.Address.refresh_transactions` to get any new transactions since the
    object was created.

    Args:
        address_hash (:obj:`str`): The bitcoin wallet address to fetch the object for.

    Returns:
        :obj:`.Address` object representing the bitcoin wallet.
    """
    return Address.from_dict(http.get_raw_address(address_hash))


class Address:
    """
    Represents the bitcoin wallet for a address hash.

    Args:
        payload (:obj:`dict`): Json payload from which the object was built.
        address_hash (:obj:`str`): Hex address of the bitcoin wallet.
        hash160 (:obj:`str`): 20 bytes hex hash of the public key.
        total_received (:obj:`int`): Total amount received to the wallet in Satoshi.
        total_sent (:obj:`int`): Total amount sent from the wallet in Satoshi.
        final_balance (:obj:`int`): Wallet balance. Considered to be the difference between ``total_received`` and ``total_sent``.
        transactions (:obj:`typing.List` [ :obj:`.transaction.Transaction` ]): Up to the last 50 transactions for the address.
    """

    def __init__(
        self,
        payload: dict,
        address_hash: str,
        hash160: str,
        total_received: int,
        total_sent: int,
        final_balance: int,
        transactions: typing.List[Transaction],
    ):
        self.payload = payload
        self.hash = address_hash
        self.hash160 = hash160
        self.total_received = total_received
        self.total_sent = total_sent
        self.balance = final_balance
        self.transactions = transactions

    def __str__(self):
        return self.hash

    def __repr__(self):
        return f"<Address ({self.hash}) balance={self.balance}>"

    @classmethod
    def from_dict(cls, payload: dict) -> Address:
        """
        Instantiate a :obj:`.Address` object from the json payload supplied by the blockchain API.

        Args:
            payload (:obj:`dict`): JSON data received from the blockchain API.

        Returns:
            :obj:`.Address` object representing the bitcoin wallet.
        """
        return cls(
            payload,
            payload["address"],
            payload["hash160"],
            payload["total_received"],
            payload["total_sent"],
            payload["final_balance"],
            [Transaction.from_dict(raw_tx) for raw_tx in payload["txs"]],
        )

    def refresh_transactions(self) -> typing.List[Transaction]:
        """
        Re-request the data for the wallet from the API and update the required attributes if
        any data has changed.

        Returns:
             A :obj:`typing.List` [ :obj:`.transaction.Transaction` ] containing up to the last 50 transactions for the address.
        """
        data = http.get_raw_address(self.hash)
        self.total_received, self.total_sent = (
            data["total_received"],
            data["total_sent"],
        )
        self.balance = data["final_balance"]
        self.transactions = [Transaction.from_dict(tx) for tx in data["txs"]]
        return self.transactions

    def get_transactions(
        self, limit: int = 50, offset: int = None
    ) -> typing.List[Transaction]:
        """
        Get a specific number and selection of transactions for the bitcoin address.

        Args:
            limit (:obj:`int`): Number of transactions to fetch. Max 50. Defaults to 50 if not supplied.
            offset (:obj:`int`): Number of transactions to offset response by. Eg, if 50, will get transactions after the most recent 50.

        Returns:
            A :obj:`typing.List` [ :obj:`.transaction.Transaction` ] containing the requested transactions for the address.
        """
        data = http.get_raw_address(self.hash, limit, offset)
        return [Transaction.from_dict(tx) for tx in data["txs"]]
