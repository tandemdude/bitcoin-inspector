from __future__ import annotations

import typing

import bitcoin_inspector.http as http
from .transaction import Transaction


def get_block(block_hash: str) -> Block:
    """
    Get a :obj:`.Block` object for a given block hash.

    Args:
        block_hash (:obj:`str`): The block hash to fetch the object for.

    Returns:
        :obj:`.Block` object representing the block.
    """
    return Block.from_dict(http.get_raw_block(block_hash))


def get_blocks_at_height(block_height: int) -> typing.List[Block]:
    """
    Get all :obj:`.Block` object for a given block height.

    Args:
        block_height (:obj:`str`): The block height to fetch the blocks for.

    Returns:
        :obj:`typing.List` [ :obj:`.Block` ] representing the blocks at that height.
    """
    return [
        Block.from_dict(raw_block)
        for raw_block in http.get_raw_blocks_at_height(block_height)
    ]


class Block:
    """
    Represents a block on the blockchain.

    Transactions for the block will attempt to be parsed into a :obj:`.transaction.Transaction`
    object. Successfully parsed transactions can be found in :attr:`.Block.transactions`. If any
    transactions could not be parsed then you can access the json payload directly from the
    :attr:`.Block.unparsed_transactions` attribute.

    Args:
        payload (:obj:`dict`): Json payload from which the object was built.
        block_hash (:obj:`str`): The hex hash of the block.
        previous_block_hash (:obj:`str`): The hex hash of the previous block in the chain.
        merkle_root (:obj:`str`): Root of the block's merkle tree.
        time (:obj:`int`): The time it took to create the block.
        nonce (:obj:`int`): The block's nonce.
        size (:obj:`int`): The size of the transaction in bytes.
        main_chain (:obj:`bool`): If the block is in the main chain or not.
        height (:obj:`int`): The height of the block.
        weight (:obj:`int`): The weight of the block
        transactions (:obj:`typing.List` [ :obj:`dict` ]): List of raw transactions in the block in json format.
    """

    def __init__(
        self,
        payload: dict,
        block_hash: str,
        previous_block_hash: str,
        merkle_root: str,
        time: int,
        nonce: int,
        size: int,
        main_chain: bool,
        height: int,
        weight: int,
        transactions: typing.List[dict],
    ):
        self.raw_payload = payload
        self.hash = block_hash
        self.previous_block_hash = previous_block_hash
        self.merkle_root = merkle_root
        self.time = time
        self.nonce = nonce
        self.size = size
        self.main_chain = main_chain
        self.height = height
        self.weight = weight
        self.transactions, self.unparsed_transactions = self.parse_raw_transactions(
            transactions
        )

    @classmethod
    def from_dict(cls, payload: dict) -> Block:
        """
        Instantiate a :obj:`.Block` object from the json payload supplied by the blockchain API.

        Args:
            payload (:obj:`dict`): JSON data received from the blockchain API.

        Returns:
            :obj:`.Block` object representing the block.
        """
        return cls(
            payload,
            payload["hash"],
            payload["prev_block"],
            payload["mrkl_root"],
            payload["time"],
            payload["nonce"],
            payload["size"],
            payload["main_chain"],
            payload["height"],
            payload["weight"],
            payload["tx"],
        )

    @staticmethod
    def parse_raw_transactions(
        transactions: typing.List[dict],
    ) -> typing.Tuple[typing.List[Transaction], typing.List[dict]]:
        """
        Parses each transaction from the payload into a :obj:`.transaction.Transaction` object. If
        it fails to parse one of the transactions then it appends it to a different list and continues
        to the next transaction in the list.

        Args:
            transactions (:obj:`typing.List` [ :obj:`dict` ]): Raw transactions to parse.

        Returns:
            :obj:`typing.Tuple` [ :obj:`typing.List` [ :obj:`.transaction.Transaction` ], :obj:`typing.List` [ :obj:`dict` ]]
            
            Tuple contains the list of :obj:`.transaction.Transaction` objects for each transaction that was successfully
            parsed, and a list of :obj:`dict` representing the transactions for which the parsing failed.
        """
        parsed_transactions, unparsed_transactions = [], []
        for transaction in transactions:
            try:
                parsed_transaction = Transaction.from_dict(transaction)
                parsed_transactions.append(parsed_transaction)
            except KeyError:
                unparsed_transactions.append(transaction)
        return parsed_transactions, unparsed_transactions
