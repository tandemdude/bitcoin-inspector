import requests


def get_current_block_height():
    resp = requests.get("https://blockchain.info/latestblock")
    resp.raise_for_status()
    data = resp.json()
    return data["height"]


def get_raw_transaction(transaction_hash):
    resp = requests.get(f"https://blockchain.info/rawtx/{transaction_hash}")
    resp.raise_for_status()
    return resp.json()


def get_raw_block(block_hash):
    resp = requests.get(f"https://blockchain.info/rawblock/{block_hash}")
    resp.raise_for_status()
    return resp.json()


def get_raw_blocks_at_height(block_height):
    resp = requests.get(
        f"https://blockchain.info/block-height/{block_height}?format=json"
    )
    resp.raise_for_status()
    data = resp.json()
    return data["blocks"]


def get_raw_address(address_hash, limit=None, offset=None):
    params = {}
    if limit is not None:
        params["n"] = limit
    if offset is not None:
        params["offset"] = offset
    resp = requests.get(
        f"https://blockchain.info/rawaddr/{address_hash}", params=params
    )
    resp.raise_for_status()
    return resp.json()


def get_exchange_rates():
    resp = requests.get("https://blockchain.info/ticker")
    resp.raise_for_status()
    return resp.json()
