import mock

import bitcoin_inspector as btc_in
from bitcoin_inspector import http


def test_convert_satoshi_to_bitcoin():
    bitcoin_amount = btc_in.utils.convert_satoshi_to_bitcoin(1234567)
    assert bitcoin_amount == 0.01234567


def test_get_bitcoin_exchange_rate():
    payload = {"USD": {"last": 8682.53}}
    mock_response = mock.MagicMock(return_value=payload)
    with mock.patch.object(http, "get_exchange_rates", new=mock_response) as response:
        exchange_rate = btc_in.utils.get_bitcoin_exchange_rate("USD")

    assert exchange_rate == payload["USD"]["last"]
