import mock
import pytest

import bitcoin_inspector as btc_in
from bitcoin_inspector import http


@pytest.fixture
def simple_transaction():
    tx_in = btc_in.models.transaction.TransactionInput(
        {"stuff": "stuff"}, "12345abcde", 5000
    )
    tx_out = btc_in.models.transaction.TransactionOutput(
        {"stuff": "stuff"}, "abcde12345", 4000
    )
    transaction = btc_in.models.transaction.Transaction(
        {"stuff": "stuff"}, "1a2b3c4d5e", 123, 123, [tx_in], [tx_out]
    )
    return transaction


def test_create_transaction_object_from_json_payload():
    payload = {
        "ver": 1,
        "inputs": [
            {
                "sequence": 4294967295,
                "witness": "",
                "prev_out": {
                    "spent": True,
                    "spending_outpoints": [{"tx_index": 0, "n": 0}],
                    "tx_index": 0,
                    "type": 0,
                    "addr": "1PcqTiRc4niMGTcAdf4YfJf2WhtgQnmynR",
                    "value": 18150713,
                    "n": 1,
                    "script": "76a914f819c75d66ad07f4e49801b010aea2919aed555188ac",
                },
                "script": "47304402204d21a8b0800d7db56e8184411822b33212425d9a0f92a6ece8db54a4ed32505702205f4e8c298283966b9cf5aa29d1188d8671fa20d412460ac869d08cd31f68a92a01210399faa8776b306cb068eef9c0a81ded9453a840fc0f1752ae5cad1e167beb80f9",
            }
        ],
        "weight": 892,
        "block_height": 629776,
        "relayed_by": "0.0.0.0",
        "out": [
            {
                "spent": False,
                "tx_index": 0,
                "type": 0,
                "addr": "34atBuGiSXdCHaCFZLa3cfxnu14uPYiFUM",
                "value": 576454,
                "n": 0,
                "script": "a9141fbf6cafe8e2c2a39284e26d9977b46d648035f787",
            },
            {
                "spent": True,
                "spending_outpoints": [{"tx_index": 0, "n": 0}],
                "tx_index": 0,
                "type": 0,
                "addr": "1J1vQkPEPZDbrEfHopj16iVBccna9yUg8d",
                "value": 17573590,
                "n": 1,
                "script": "76a914baa6b8e7c01ed350be3a65f5b146bca703699fce88ac",
            },
        ],
        "lock_time": 0,
        "size": 223,
        "block_index": 0,
        "time": 1589109771,
        "tx_index": 0,
        "vin_sz": 1,
        "hash": "b3a41012c310cbb732b724f3c5b1a6bce20330ec5d273f0068364ba810aae66b",
        "vout_sz": 2,
    }
    tx_obj = btc_in.models.transaction.Transaction.from_dict(payload)

    assert tx_obj.payload == payload
    assert tx_obj.hash == payload["hash"]
    assert tx_obj.hyperlink == f"https://www.blockchain.com/btc/tx/{payload['hash']}"
    assert tx_obj.size == payload["size"]
    assert tx_obj.block_height == payload["block_height"]
    assert (
        isinstance(tx_obj.inputs[0], btc_in.models.transaction.TransactionInput) is True
    )
    assert (
        isinstance(tx_obj.outputs[0], btc_in.models.transaction.TransactionOutput)
        is True
    )


def test_create_transaction_output_object_from_json_payload():
    payload = {
        "spent": False,
        "tx_index": 0,
        "type": 0,
        "addr": "34atBuGiSXdCHaCFZLa3cfxnu14uPYiFUM",
        "value": 576454,
        "n": 0,
        "script": "a9141fbf6cafe8e2c2a39284e26d9977b46d648035f787",
    }
    tx_output_obj = btc_in.models.transaction.TransactionOutput.from_dict(payload)

    assert tx_output_obj.payload == payload
    assert tx_output_obj.value == payload["value"]
    assert tx_output_obj.address == payload["addr"]


def test_create_transaction_input_object_from_json_payload():
    payload = {
        "sequence": 4294967295,
        "witness": "",
        "prev_out": {
            "spent": True,
            "spending_outpoints": [{"tx_index": 0, "n": 0}],
            "tx_index": 0,
            "type": 0,
            "addr": "1PcqTiRc4niMGTcAdf4YfJf2WhtgQnmynR",
            "value": 18150713,
            "n": 1,
            "script": "76a914f819c75d66ad07f4e49801b010aea2919aed555188ac",
        },
        "script": "47304402204d21a8b0800d7db56e8184411822b33212425d9a0f92a6ece8db54a4ed32505702205f4e8c298283966b9cf5aa29d1188d8671fa20d412460ac869d08cd31f68a92a01210399faa8776b306cb068eef9c0a81ded9453a840fc0f1752ae5cad1e167beb80f9",
    }
    tx_input_obj = btc_in.models.transaction.TransactionInput.from_dict(payload)

    assert tx_input_obj.payload == payload
    assert tx_input_obj.address == payload["prev_out"]["addr"]
    assert tx_input_obj.value == payload["prev_out"]["value"]


def test_calculate_confirmations_for_transaction(simple_transaction):
    transaction = simple_transaction

    mock_response = mock.MagicMock(return_value=133)

    with mock.patch.object(
        http, "get_current_block_height", new=mock_response
    ) as response:
        confirmations = transaction.confirmations
    assert confirmations == 10


def test_calculate_fees_for_transaction(simple_transaction):
    assert simple_transaction.fee == 1000
