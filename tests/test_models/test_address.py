import mock
import pytest

import bitcoin_inspector as btc_in


def test_create_address_object_from_json_payload():
    payload = {
        "hash160": "bb2c1aadaf385217ebe26dbc362ec652413ace6b",
        "address": "3Jkh7q7wEzJqRdFzVwVKeZzxY6YA5EGz23",
        "n_tx": 1,
        "total_received": 528450,
        "total_sent": 0,
        "final_balance": 528450,
        "txs": [
            {
                "ver": 1,
                "inputs": [
                    {
                        "sequence": 4294967295,
                        "witness": "",
                        "prev_out": {
                            "spent": True,
                            "spending_outpoints": [{"tx_index": 0, "n": 0}],
                            "tx_index": 0,
                            "type": 0,
                            "addr": "1FWFy5g6kh4J7PysTSGgqxhzwC8yRvLa6e",
                            "value": 10732670,
                            "n": 1,
                            "script": "76a9149f1a514ed7e75b5d19d33e8e266a3e2f713debbd88ac",
                        },
                        "script": "47304402205ee01392ffbd46fcf86ed7d570304c583606167df840b48fbe0f09682de5b5d102200f9358aa9212c5fb606e006c8d589cc1f70ba8bfa23aa17bf8851cc7b33d55ea0121030e642277b9077aafd7e563f40c01faa32573e0c9e39ea1731f8f0900ea48612e",
                    }
                ],
                "weight": 892,
                "block_height": 629928,
                "relayed_by": "0.0.0.0",
                "out": [
                    {
                        "spent": False,
                        "tx_index": 0,
                        "type": 0,
                        "addr": "3Jkh7q7wEzJqRdFzVwVKeZzxY6YA5EGz23",
                        "value": 528450,
                        "n": 0,
                        "script": "a914bb2c1aadaf385217ebe26dbc362ec652413ace6b87",
                    },
                    {
                        "spent": True,
                        "spending_outpoints": [{"tx_index": 0, "n": 0}],
                        "tx_index": 0,
                        "type": 0,
                        "addr": "1AJRmXLCzSpWSrKnyVrZDRxDtTnUB7JjYo",
                        "value": 10203551,
                        "n": 1,
                        "script": "76a9146604e0ddf2e0dd19f30e1ee892ffae178e205e6888ac",
                    },
                ],
                "lock_time": 0,
                "result": 528450,
                "size": 223,
                "block_index": 0,
                "time": 1589183725,
                "tx_index": 0,
                "vin_sz": 1,
                "hash": "566928e28a08275cab948774379ec3d8f8c0d84b99ead0ece4f8bc55271ccd8f",
                "vout_sz": 2,
            }
        ],
    }
    address_obj = btc_in.models.address.Address.from_dict(payload)

    assert address_obj.payload == payload
    assert address_obj.hash == payload["address"]
    assert address_obj.hash160 == payload["hash160"]
    assert address_obj.total_received == payload["total_received"]
    assert address_obj.total_sent == payload["total_sent"]
    assert address_obj.balance == payload["final_balance"]
    assert (
        isinstance(address_obj.transactions[0], btc_in.models.transaction.Transaction)
        is True
    )


def test_address_refresh_transactions():
    pass


def test_address_get_transactions():
    pass
