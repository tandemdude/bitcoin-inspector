import nox
import os

PATH_TO_PROJECT = os.path.join(".", "bitcoin_inspector")
PATH_TO_TESTS = os.path.join(".", "tests")


@nox.session(python=["3.8"])
def test(session):
    session.run("pip", "install", "-Ur", "requirements.txt")
    session.run("python", "-m", "pytest", "tests", "--testdox")


@nox.session(python=["3.8"])
def format_fix(session):
    session.run("pip", "install", "-U", "black")
    session.run("python", "-m", "black", PATH_TO_PROJECT, PATH_TO_TESTS)


@nox.session(python=["3.8"])
def format(session):
    session.run("pip", "install", "-U", "black")
    session.run("python", "-m", "black", PATH_TO_PROJECT, PATH_TO_TESTS, "--check")


@nox.session(python=["3.8"])
def sphinx(session):
    session.run("pip", "install", "-U", "sphinx", "sphinx_rtd_theme")
    session.run("python", "-m", "sphinx.cmd.build", "docs/source", "docs/build", "-b", "html")
