.. Bitcoin Inspector documentation master file, created by
   sphinx-quickstart on Sun May 10 19:43:22 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg

----

Bitcoin Inspector
=================

Python library intended to provide a simple method for fetching and interacting with data for bitcoin transactions, blocks, and addresses.

This project is currently in pre-alpha. This means that documentation may not be complete and the whole library may not be completely tested at the current time.

Repository: `View on GitLab <https://gitlab.com/tandemdude/bitcoin-inspector>`_

Docs: `View Here <https://tandemdude.gitlab.io/bitcoin-inspector/index.html>`_

.. toctree::
    :maxdepth: 2

    api-reference

* :ref:`genindex`
